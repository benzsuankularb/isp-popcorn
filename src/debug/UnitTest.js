var assert = function( actual ,expected ,message ) {
    var passCss = "color: hsl( 104, 100%, 47% ); font-size: 12px;";
    var failCss = "color: hsl( 360, 100%, 43% ); font-size: 12px;";
    if ( actual === expected || JSON.stringify( actual ) == JSON.stringify( expected ) ){
	console.log ( '%cPASS %6s %30s' ,passCss ,message , 'answer is ' + JSON.stringify( actual ) );
    } else {
	console.log ( '%cFAIL %6s %30s' ,failCss ,message , 'answer is ' + JSON.stringify( actual ) );
    }
};

var assertHeader = function ( header ) {
    this.css = "color: rgb(30, 30, 200); font-size: 12px;";
    console.log( '%cTesting : ' + header ,this.css );
}