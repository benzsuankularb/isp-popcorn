var MapDebug = Map.extend ({
    
    ctor: function ( mapArray, elementSize ) {
        this.imgElements = [];
        this.img = TextureCache.addImage("res/debug/Map.png");
        Map.prototype.ctor.call( this, mapArray, elementSize );
    },
    
    _createElementAt: function ( indexX, indexY ) {
        Map.prototype._createElementAt.call( this, indexX , indexY );
        if( this.imgElements[indexX] == null )
            this.imgElements[indexX] = [];
        var sprite = new cc.Sprite();
        sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
        sprite.setPosition( cc.rectCenter( this.getElementAt( indexX, indexY ).rectArea ) );
        this.imgElements[indexX][indexY] = sprite;
        this._setOpacity( indexX, indexY, 50 );
        this.addChild( sprite );
    },
    
    _setOpacity: function ( indexX, indexY , amount ) {
        this.imgElements[indexX][indexY].setOpacity( amount );
    },
    
    notifyMove: function ( component ) {
        Map.prototype.notifyMove.call( this, component );
        for (var i = 0 ; i < this.indexX ; i++) {
            for (var j = 0 ; j < this.indexY ; j++) {
                var element = this.getElementAt( i, j );
                if ( element.reservedBlockComponent ) {
                    this._setOpacity( i, j , 180 );
                } else if ( element.getOverlapComponents().length > 0 ) {
                    this._setOpacity( i, j , 100 );
                } else {
                    this._setOpacity( i, j , 50 );
                }
            }
        }
    }
});