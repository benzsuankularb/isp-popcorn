var SpriteFrameCache = cc.SpriteFrameCache.getInstance();
var AnimationCache = cc.AnimationCache.getInstance();
var TextureCache = cc.TextureCache.getInstance();
var isDebug = false;//(document.ccConfig.COCOS2D_DEBUG != 0);

var Sound = {
   music1 : "res/sounds/Music1.mp3",
   BounceFx : "res/sounds/BounceFx.wav",
   BoomFx : "res/sounds/BoomFx.wav",
   CatchedFx : "res/sounds/CatchedFx.wav",
   ConcreteFx : "res/sounds/ConcreteFx.wav",
   DieFx : "res/sounds/DieFx.wav",
   MergeFx : "res/sounds/MergeFx.wav",
   PushFx : "res/sounds/PushFx.wav",
   SpringFx : "res/sounds/SpringFx.wav",
   StartFx : "res/sounds/StartFx.wav",
   SquashFx : "res/sounds/SquashFx.wav",
   WinFx : "res/sounds/WinFx.wav",
   ExplodeFx : "res/sounds/ExplodeFx.wav",
   PopFx : "res/sounds/PopFx.wav",
   MatchFx : "res/sounds/MatchFx.wav"
}

var g_ressources = [
    //image
    {src: 'res/textures/prop_atlas.png' },
    {src: 'res/images/background.jpg' },
    {src: 'res/images/character.png'},
    {src: 'res/debug/Map.png' },
    {src: 'res/debug/Ball.png'},
 
    //plist
    {src: 'res/textures/prop_atlas.plist' },
    
    //FX
    {type:"effect",src:"res/sounds/BoomFx.wav"},
    {type:"effect",src:"res/sounds/ExplodeFx.wav"},
    {type:"effect",src:"res/sounds/BounceFx.wav"},
    {type:"effect",src:"res/sounds/CatchedFx.wav"},
    {type:"effect",src:"res/sounds/ConcreteFx.wav"},
    {type:"effect",src:"res/sounds/DieFx.wav"},
    {type:"effect",src:"res/sounds/MergeFx.wav"},
    {type:"effect",src:"res/sounds/PushFx.wav"},
    {type:"effect",src:"res/sounds/SpringFx.wav"},
    {type:"effect",src:"res/sounds/SquashFx.wav"},
    {type:"effect",src:"res/sounds/StartFx.wav"},
    {type:"effect",src:"res/sounds/WinFx.wav"},
    {type:"effect",src:"res/sounds/PopFx.wav"},
    {type:"effect",src:"res/sounds/MatchFx.wav"},
    
    //music
    {type:"bgm",src: Sound.music1 }
];