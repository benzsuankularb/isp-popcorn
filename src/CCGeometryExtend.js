cc.Circ = function ( x1 ,y1 ,radius ) {
    this._radius = radius;
    this._origin = cc.p( x1, y1);
};

cc.circ = function ( x1 ,y1 , radius ) {
    return new cc.Circ( x1, y1, radius );
}

Object.defineProperties(cc.Circ.prototype , {
    "x": {
        get: function () {
            return this._origin.x;
        },
        set: function (x) {
            return this._origin.x = x;
        },
        enumerable: true,
        configurable: true
    },
    "y": {
        get: function () {
            return this._origin.y;
        },
        set: function (y) {
            return this._origin.y = y;
        },
        enumerable: true,
        configurable: true
    },
    "radius": {
        get: function () {
            return this._radius;
        },
        set: function ( radius ) {
            return this._radius = radius;
        },
        enumerable: true,
        configurable: true
    }
});

cc.Line = function ( p1, p2 ) {
    this._p1 = p1;
    this._p2 = p2;
}

cc.line = function ( p1, p2 ) {
    return new cc.Line(p1, p2);
}

Object.defineProperties(cc.Line.prototype , {
    "first": {
        get: function () {
            return this._p1;
        },
        set: function (p1) {
            return this._p1 = p1;
        },
        enumerable: true,
        configurable: true
    },
    "last": {
        get: function () {
            return this._p2;
        },
        set: function (p2) {
            return this._p2 = p2;
        },
        enumerable: true,
        configurable: true
    }
});

cc.lineIntersectsCirc = function ( line, circ ) {
    var point = cc.linePerpendicularPointFromPoint( line, circ._origin );
    var len = cc.pointsMagnitute( point, circ._origin );
    var minX = Math.min( line.first.x , line.last.x );
    var maxX = Math.max( line.first.x , line.last.x );
    var minY = Math.min( line.first.y , line.last.y );
    var maxY = Math.max( line.first.y , line.last.y );
    
    return len <= circ.radius && point.x >= minX && point.x <= maxX && point.y >= minY && point.y <= maxY;
}

//Ref : http://stackoverflow.com/questions/10301001/perpendicular-on-a-line-segment-from-a-given-point
cc.linePerpendicularPointFromPoint = function ( line, point ){
    var x1 = line.first.x, y1 = line.first.y, x2 = line.last.x, y2 = line.last.y, x3 = point.x, y3 = point.y;
    var px = x2-x1, py = y2-y1, dAB = px*px + py*py;
    var u = ((x3 - x1) * px + (y3 - y1) * py) / dAB;
    var x = x1 + u * px, y = y1 + u * py;
    return cc.p( x, y );
}

N = function ( p ) {
    var aTan = Math.atan( p.y / p.x );
    if( p.x >= 0 ) {
        if ( p.y >= 0 ) { //Q2
            return 90 - aTan;
        } else { //Q4
            return 90 + aTan;
        }
    } else {
        if ( p.y >= 0 ) { //Q1
            return 270 + aTan;
        } else { //Q3
            return 270 + aTan;
        }
    }
}

cc.rectIntersectsCirc = function ( rect, circ ) {
    var p1, p2, p3, p4;
    p1 = cc.p( cc.rectGetMinX( rect ) , cc.rectGetMinY( rect ) );
    p2 = cc.p( cc.rectGetMinX( rect ) , cc.rectGetMaxY( rect ) );
    p3 = cc.p( cc.rectGetMaxX( rect ) , cc.rectGetMaxY( rect ) );
    p4 = cc.p( cc.rectGetMaxX( rect ) , cc.rectGetMinY( rect ) );
    pCenter = cc.rectCenter( rect );
    var AB = cc.line( p1, p2 );
    var BC = cc.line( p2, p3 );
    var CD = cc.line( p3, p4 );
    var DA = cc.line( p4, p1 );
    return ( cc.circContainsPoint( circ, p1 ) || cc.circContainsPoint( circ, p2 ) || cc.circContainsPoint( circ, p3 ) || cc.circContainsPoint( circ, p4 ) || cc.lineIntersectsCirc( AB, circ ) || cc.lineIntersectsCirc( BC, circ ) || cc.lineIntersectsCirc( CD, circ ) || cc.lineIntersectsCirc( DA, circ) || cc.rectContainsPoint( rect, circ._origin ) );
}

cc.rectContainsCirc = function ( rect, circ ) {
    return circ.x - circ._radius  > cc.rectGetMinX( rect ) &&
            circ.x + circ._radius < cc.rectGetMaxX( rect ) &&
            circ.y - circ._radius  > cc.rectGetMinY( rect ) &&
            circ.y + circ._radius < cc.rectGetMaxY( rect );
}

cc.circIntersectsCirc = function ( circ1, circ2 ) {
    return circ1.radius + circ2.radius >= cc.pointsMagnitute( circ1._origin, circ2._origin );
}

cc.circContainsPoint = function ( circle, point ) {
    return cc.pointsMagnitute( circle._origin, point) <= circle.radius;
}

cc.pointsMagnitute = function ( p1, p2 ) {
    return Math.sqrt( Math.pow( (p2.x - p1.x), 2) + Math.pow( (p2.y - p1.y), 2 ) , 2 );
}

cc.pMinus = function ( p1, p2 ) {
    return cc.p( p1.x - p2.x, p1.y - p2.y );
}

cc.pPlus = function ( p1, p2 ) {
    return cc.p( p1.x + p2.x, p1.y + p2.y );
}

cc.rectCenter = function ( rect ) {
    return cc.p( cc.rectGetMidX( rect ) , cc.rectGetMidY( rect ) );
}

Array.prototype.indexOf = function( item ) {
    var count = this.length;
    var j = 0;
    while (this[j] !== item && j < count) { j++; }
    return (j === count) ? -1 : j;
}

var assertGeo = function(){
    assertHeader( "Geometry and algorithm.")
    assert( cc.pointsMagnitute( cc.p( 0, 0 ) ,cc.p( 3, 4 ) ) , 5 , "magnitute point" );
    assert( cc.circIntersectsCirc( cc.circ( 0 ,0 ,2 ) ,cc.circ( 3, 3, 1 ) ) ,false ,"intersect of circle" );
    assert( cc.circIntersectsCirc( cc.circ( 10, 10, 5 ) , cc.circ( 20, 20, 10 ) ) ,true ,"unintersect of circle" );
    assert( cc.circIntersectsCirc( cc.circ( 0, 0, 2 ) ,cc.circ( 3, 4, 3 ) ) ,true ,"unintersect of circle" );
    assert( cc.circIntersectsCirc( cc.circ( 0, 0, 1.5 ) , cc.circ( 3, 4, 3 ) ) ,false ,"unintersect of circle" );
    assert( cc.linePerpendicularPointFromPoint( cc.line( cc.p( 10, 0 ) , cc.p( 10, 20 ) ) ,cc.p( 5, 5 ) ) ,cc.p( 10, 5 ) ,"line normal point" );
    assert( cc.linePerpendicularPointFromPoint( cc.line( cc.p( 10, 0 ) , cc.p( 10, 20 ) ) ,cc.p( 5, 6 ) ) ,cc.p( 10, 6 ) ,"line normal point" );
    assert( cc.lineIntersectsCirc( cc.line( cc.p( 10, 0 ) , cc.p (10, 20 ) ) , cc.circ( 5, 5, 5 ) ) ,true ,"line intersec circle" );
    assert( cc.lineIntersectsCirc( cc.line( cc.p( 10, 0 ) , cc.p( 10, 3 ) ) , cc.circ( 5, 5, 5 ) ) ,false ,"line intersec circle" );
    assert( cc.rectIntersectsCirc( cc.rect( 0 ,0 ,4 ,4 ) , cc.circ( 6, 2, 1.9 ) ) , false , "Rect intersec circle");
    assert( cc.rectIntersectsCirc( cc.rect( 0 ,0 ,4 ,4 ) , cc.circ( 6, 2, 2 ) ) , true , "Rect intersec circle");
    assert( cc.rectIntersectsCirc( cc.rect( 0 ,0 ,4 ,4 ) , cc.circ( 2, 2, 1 ) ) , true , "Rect intersec circle");
    assert( cc.rectIntersectsCirc( cc.rect( 0 ,0 ,4 ,4 ) , cc.circ( 2, 2, 10 ) ) , true , "Rect intersec circle");
}