var GameLayer = cc.Layer.extend({
    init: function() {
        this._super();
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setKeyboardEnabled( true );
        this.setTouchEnabled( true );
        
        /* init Music */
        cc.AudioEngine.getInstance().setMusicVolume( 0.5 );
        cc.AudioEngine.getInstance().setEffectsVolume( 0.5 );
        cc.AudioEngine.getInstance().playMusic( Sound.music1 ,true );
        
        /* init GameObject*/
        var map = new PropMap([[1,0,0,0,1,0,1,1,1,1,0,1,1,1,0,1],
                                 [1,0,1,0,1,0,1,0,0,0,0,0,0,3,0,1],
                                 [1,0,1,3,1,0,1,0,1,1,0,2,0,1,0,1],
                                 [1,0,0,0,0,0,4,0,0,1,0,1,0,0,0,0],
                                 [1,1,1,1,1,0,0,0,0,1,0,1,1,1,1,0],
                                 [0,0,0,0,0,0,1,3,1,2,0,0,0,0,0,0],
                                 [1,1,1,1,1,0,1,0,0,0,0,1,1,1,0,1],
                                 [1,0,0,0,4,0,0,0,1,1,0,4,0,0,0,1],
                                 [1,0,2,0,1,0,1,0,0,2,0,1,0,3,0,1],
                                 [1,0,1,0,0,0,2,1,0,1,0,1,0,1,0,0],
                                 [1,1,1,1,1,0,0,0,0,1,0,0,0,3,1,0]] , GameProperties.ELEMENT_SIZE );
        this.addChild(map);
        this.myComponents = [];
        this.c1 = new Character( map , GameProperties.GetComponentRadius() , 8, 0 );
        map.addCharacterAt( this.c1 , cc.p( 15, 0 ) );
        this.joy = new KeyboardJoystick( this.c1, KeyboardJoystick.DEFAULT_KEY_OBJECT );
        this.c3 = new Character( map , GameProperties.GetComponentRadius() , 8, 0 );
        map.addCharacterAt( this.c3 , cc.p( 1, 1 ) );
        this.joy3 = new KeyboardJoystick( this.c3, KeyboardJoystick.DEFAULT_KEY_OBJECT3 );
        this.c2 = new Character( map , GameProperties.GetComponentRadius() , 8, 0 );
        map.addCharacterAt( this.c2 , cc.p( 1, 10 ) );
        this.joy2 = new KeyboardJoystick( this.c2, KeyboardJoystick.DEFAULT_KEY_OBJECT2 );
        map.setScale( 0.632 );
        
        /* Assert */
        if ( isDebug ) {
            assertGeo();
            //assertMap( debugMap );
        }
    },
    
    onKeyDown: function( e ) {
        this.joy.onKeyDown( e );
        this.joy2.onKeyDown( e );
    },
    
    onKeyUp: function( e ) {
        this.joy.onKeyUp( e );
        this.joy2.onKeyUp( e );
    },
    
    //onTouchE
});

var BackgroundLayer = cc.Layer.extend({
    init : function() {
        this._super();
        this.setPosition( new cc.Point( 0, 0 ) );
        this.bgSprite = new cc.Sprite();
        this.bgSprite.initWithFile( "res/images/background.jpg");
        this.bgSprite.setScale( 1.5 );
        this.addChild( this.bgSprite );
        this.bgSprite.setAnchorPoint( 0, 0 );
        this.bgSprite.setPosition( 0, 0 );
        
    }
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        Assets.getInstance();
        PropFactory.getInstance();
        
        var bgLayer = new BackgroundLayer();
        bgLayer.init();
        this.addChild( bgLayer );
        
        var gameLayer = new GameLayer();
        gameLayer.init();
        this.addChild( gameLayer );
    },
    
    centerSpriteFrameOffset: function ( spriteFrame ) {
        spriteFrame.setOffset( cc.p( -spriteFrame.getOriginalSize().width/2, -spriteFrame.getOriginalSize().height/2 ) );
    },
    
    getAnimationFromSpriteCache: function ( prefix, count ) {
        var animationArray = [];
        SpriteFrameCache.getSpriteFrame( "" );
    }
});