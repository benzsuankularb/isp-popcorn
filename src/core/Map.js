var Map = cc.Sprite.extend ({
    
    ctor: function ( mapArray, elementSize ) {
        cc.Sprite.prototype.ctor.call(this);
        this.mapElements = [];
        this.components = [];
        this.elementSize = elementSize;
        this.width = mapArray[0].length * this.elementSize;
        this.height = mapArray.length * this.elementSize;
        this.indexY = mapArray.length;
        this.indexX = mapArray[0].length;
        this.rectArea = new cc.Rect( 0, 0 ,this.width ,this.height );
        this._initElements( mapArray );
        this._initBubbles( mapArray );
    },
    
    _initElements: function ( mapArray ) {
        var element;
        for ( var i = 0 ; i < this.indexX ; i++ ) {
            for ( var j = 0 ; j < this.indexY ; j++ ) {
                this._createElementAt( i, j );
            }
        }
    },
    
    _initBubbles: function ( mapArray ) {
        for ( var i = 0 ; i < this.indexX ; i++ ) {
            for ( var j = 0 ; j < this.indexY ; j++ ) {
                this._createBubbleAt( i, j , mapArray[this.indexY - 1 - j][i] );
            }
        }
    },
    
    _createBubbleAt: function ( indexX , indexY, type ) {
        //TODO Change to factory pattern
        if ( type == Map.ELEMENT_TYPE.GeneralBubble ){
            this.addBlockComponentAt( new GeneralBubble( this , GameProperties.GetComponentRadius() ) , cc.p( indexX, indexY ) );
        }else if ( type == Map.ELEMENT_TYPE.BounceBubble1 ){
            this.addBlockComponentAt( new BounceBubble( this , GameProperties.GetComponentRadius() , 1 ) , cc.p( indexX, indexY ) );
        }else if ( type == Map.ELEMENT_TYPE.BombBubble1 ){
            this.addBlockComponentAt( new BombBubble( this , GameProperties.GetComponentRadius() , BombBubble.BOMB_ARRAY1 ) , cc.p( indexX, indexY ) );
        }else if ( type == Map.ELEMENT_TYPE.DiamondBubble ){
            this.addBlockComponentAt( new DiamondBubble( this , GameProperties.GetComponentRadius() ) , cc.p( indexX, indexY ) );
        }
    },
    
    _createElementAt: function ( indexX, indexY ) {
        element = new MapElement( indexX, indexY, this );
        if( this.mapElements[indexX] == null )
            this.mapElements[indexX] = [];
        this.mapElements[indexX][indexY] = element;
    },
    
    getElementAt: function ( indexX , indexY ) {
        if ( arguments.length == 1){
            return this.getElementAt( indexX.x, indexX.y);
        }else {
            if( !this.isIndexInBound( cc.p( indexX, indexY ) ) ) return null;
            return this.mapElements[indexX][indexY];
        }
    },
    
    getMapIndexByLocation: function ( mapLocation ){
        var i = 0;
        var j = 0;
        while( mapLocation.x > ( i + 1 ) * this.elementSize &&  i < this.indexX ) { i++; }
        while( mapLocation.y > ( j + 1 ) * this.elementSize &&  j < this.indexY ) { j++; }
        return cc.p( i, j );
    },
    
    getLocationAtMapIndex: function ( mapIndex ) {
        return cc.rectCenter( this.getElementAt( mapIndex.x, mapIndex.y ).rectArea );
    },
    
    addComponentAt: function ( component, mapLocation ) {
        this.addChild( component );
        this.components.push( component );
        component.setLocation( mapLocation );
    },
    
    addBlockComponentAt: function ( blockComponent, mapIndex ) {
        var element = this.getElementAt( mapIndex.x , mapIndex.y );
        this.addComponentAt ( blockComponent, cc.rectCenter( element.rectArea ) );
    },
    
    removeComponent: function ( component ) {
        var mapIndexs = component.getOverlapMapIndexs();
        var length = mapIndexs.length
        var element;
        for(var i = 0 ; i < length ; i++) {
            element = this.getElementAt( mapIndexs[i] );
            element.popOverlapComponents( component );
        }
        this.components.splice( this.components.indexOf( component ) );
        this.removeChild( component );
    },
    
    getOverlapMapIndexsOfCirc: function ( circ ) {
        var index = this.getMapIndexByLocation( cc.p( circ.x , circ.y ) );
        var minimumRing = Math.ceil( circ.radius * 1.0 / this.elementSize );
        var array = [];
        var element;
        for ( var i = index.x - minimumRing ; i <= index.x + minimumRing ; i++ ) {
            if( i < 0 || i >= this.indexX ) continue;
            for ( var j = index.y - minimumRing ; j <= index.y + minimumRing ; j++ ) {
                if( j < 0 || j >= this.indexY ) continue;
                element = this.getElementAt( i, j );
                if ( cc.rectIntersectsCirc( element.rectArea , circ) ) {
                    array.push( cc.p( i, j) );
                }
            }
        }
        return array;
    },
    
    notifyMove: function ( component ) {
        var oldMapIndexs = component.getOverlapMapIndexs();
        var newMapIndexs = this.getOverlapMapIndexsOfCirc( component.getCollider() );
        var oldLength = oldMapIndexs.length;
        var newLength = newMapIndexs.length;
        var element, newMapElement = [], i = 0;
        i = 0;
        while ( i < newLength ) {
            element = this.getElementAt( newMapIndexs[i++] );
            element.addOverlapComponents( component );
            newMapElement.push( element );
        }
        i = 0;
        while ( i < oldLength ) {
            element = this.getElementAt( oldMapIndexs[i++] );
            if ( newMapElement.indexOf( element ) == -1 ){
                element.popOverlapComponents( component );
            }
        }
        component.setOverlapMapIndexs( newMapIndexs );
    },
     
    getNextMapIndex: function ( index, dir ) {
        var returnIndex;
        if( dir == BlockComponent.DIR.UP ) {
            returnIndex = cc.p ( index.x , index.y + 1 );
        } else if ( dir == BlockComponent.DIR.DOWN ) {
            returnIndex = cc.p ( index.x , index.y - 1 );
        } else if ( dir == BlockComponent.DIR.LEFT ) {
            returnIndex = cc.p ( index.x - 1 , index.y );
        } else if ( dir == BlockComponent.DIR.RIGHT ) {
            returnIndex = cc.p ( index.x + 1 , index.y );
        }
        if ( returnIndex.x < 0 || returnIndex.y < 0 || returnIndex.x >= this.indexX || returnIndex.y >= this.indexY ){
            return null;
        }
        return returnIndex;
    },
    
    isIndexInBound: function ( index ) {
        return index.x >= 0 && index.y >=0 && index.x < this.indexX && index.y < this.indexY;
    },
    
    isPointCenterToIndex: function ( p ) {
        return cc.pointEqualToPoint( cc.rectCenter( this.getElementAt( this.getMapIndexByLocation( p ) ).rectArea ), p );
    }
});

Map.ELEMENT_TYPE = {
    NONE: 0,
    GeneralBubble: 1,
    BounceBubble1: 2,
    BombBubble1: 3,
    DiamondBubble: 4,
}

var assertMap = function ( debugMap ) {
    //assert( map.addComponentAt( new Component() ) , cc.p(10,10) );
}