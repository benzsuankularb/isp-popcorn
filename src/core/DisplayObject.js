var DisplayObject = cc.Sprite.extend({
    
    ctor: function () {
        cc.Sprite.prototype.ctor.call( this );
        var _currentAnimation = "";
        this.animations = {};
    },
    
    getAnimation: function () {
        return this._currentAnimation;
    },
    
    setAnimation: function (animationName) {
        this.stopAnimation();
        var animation = this.animations[animationName];
        if (animation instanceof cc.Action) {
            this.runAction (animation);
        } else if (animation instanceof cc.SpriteFrame) {
            this.setDisplayFrame( animation );
        } else {
            throw 'not CCSpriteFrame or CCAnimate';
        }
        this._currentAnimation = animationName;
    },
    
    addAnimation: function ( animationName , action ) {
        this.animations[animationName] = action;
    },
    
    stopAnimation: function () {
        var action = this.animations[this._currentAnimation];
        if (action instanceof cc.Action)
            this.stopAction(action);
    },
    
    swapZOrder: function ( displayObject ) {
        var _order = diamondBubble1.getZOrder();
        diamondBubble1.reorderChild( diamondBubble2.getZOrder() );
	diamondBubble2.reorderChild( _order );
    }
});