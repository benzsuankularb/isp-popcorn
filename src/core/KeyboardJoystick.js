var KeyboardJoystick = cc.Class.extend({
    
    ctor: function ( character, keyObject ) {
        this._character = character;
        this._keyObject = keyObject;
        this._dirStack = [];
    },
    
    onDirDown: function ( dir ) {
        if ( this._dirStack[0] != dir ) {
            this._dirStack.unshift( dir );
            this._character.setNextDirection( this._dirStack[0] );
        }
    },
    
    onDirUp: function ( dir ) {
        var index = this._dirStack.indexOf( dir );
        var currentDir = this._dirStack[0];
        if ( index > -1 ) {
            this._dirStack.splice( index, 1 );
        }
        if ( this._dirStack.length == 0 ) {
            this._character.setNextDirection( BlockComponent.DIR.STAY );
        } else if ( currentDir != this._dirStack[0] ) {
            this._character.setNextDirection( this._dirStack[0] );
        }
    },
    
    onKeyDown: function ( e ) {
        var dirKey =  this.getDirectionKey( e );
        if ( dirKey ) this.onDirDown( dirKey );
        else if ( e == this._keyObject.push ) this._character.push();
        else if ( e == this._keyObject.activate ) this._character.activate();
        else if ( e == this._keyObject.power ) this._character.power( true );
    },
    
    onKeyUp: function ( e ) {
        var dirKey =  this.getDirectionKey( e );
        if ( dirKey ) this.onDirUp( dirKey );
        else if ( e == this._keyObject.power ) this._character.power( false );
    },
    
    getDirectionKey: function ( e ) {
        if ( e == this._keyObject.up ) return BlockComponent.DIR.UP;
        if ( e == this._keyObject.down ) return BlockComponent.DIR.DOWN;
        if ( e == this._keyObject.left ) return BlockComponent.DIR.LEFT;
        if ( e == this._keyObject.right ) return BlockComponent.DIR.RIGHT;
        return null;
    }
    
});

KeyboardJoystick.DEFAULT_KEY_OBJECT = {
    up : cc.KEY.up,
    down : cc.KEY.down,
    left : cc.KEY.left,
    right : cc.KEY.right,
    push : cc.KEY.space,
    activate : null,
    power : null 
}

KeyboardJoystick.DEFAULT_KEY_OBJECT2 = {
    up : cc.KEY.w,
    down : cc.KEY.s,
    left : cc.KEY.a,
    right : cc.KEY.d,
    push : cc.KEY.shift,
    activate : null,
    power : null 
}

KeyboardJoystick.DEFAULT_KEY_OBJECT3 = {
    up : cc.KEY.y,
    down : cc.KEY.h,
    left : cc.KEY.g,
    right : cc.KEY.j,
    push : cc.KEY.v,
    activate : null,
    power : null 
}