var GameProperties = cc.Class.extend({
});

GameProperties.CONTENT_SIZE = cc.size( 800, 600 );//GameProperties.GetContentSize();

GameProperties.ELEMENT_SIZE = 79;

GameProperties.GetComponentRadius = function () {
   return GameProperties.ELEMENT_SIZE / 2 - 1;
}

GameProperties.GetContentSize = function () {
   var gameArea = document.getElementById('gameCanvas');
   return cc.size( gameArea.x, gameArea.y );
}