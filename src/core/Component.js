var Component = DisplayObject.extend({
    
    ctor: function ( map, colliderRad ) {
        DisplayObject.prototype.ctor.call( this );
        this._overlapMapIndexs = [];
        this.map = map;
        this._colliderRad = colliderRad;
    },
    
    getCollider: function () {
        var p = this.getLocation();
        return cc.circ( p.x, p.y , this._colliderRad );
    },
    
    getOverlapMapIndexs: function () {
        return this._overlapMapIndexs;
    },
    
    setOverlapMapIndexs: function ( newMapIndexs ) {
        this._overlapMapIndexs = newMapIndexs;
    },
    
    getLocation: function () {
        return this.getPosition();
    },
    
    setLocation: function ( location ) {
        this.setPosition( location );
        this.map.notifyMove( this );
        this.triggerCollisionComponent();
    },
    
    isCollision: function ( component ) {
        return cc.circIntersectsCirc( this.getCollider(), component.getCollider() );
    },
    
    onCollision: function ( component ) {
        console.warn( 'Abstract not implemented onCollision()' );
    },
    
    onCollisionBound: function ( component ) {
        console.warn( 'Abstract not implemented onBound()' );
    },
    
    triggerCollisionComponent: function () {
        var components = this.getCloseComponents();
        for( var i = 0 ; i < components.length ; i++ ) {
            if ( cc.circIntersectsCirc( this.getCollider(), components[i].getCollider() ) ) {
                this.onCollision( components[i] );
            }
        }
        if ( !cc.rectContainsCirc( this.map.rectArea, this.getCollider() ) ) {
            this.onCollisionBound();
        }
    },
    
    getCloseComponents: function() {
        var elementsLength = this._overlapMapIndexs.length;
        var components;
        var returnComponents = [];
        var element;
        for( var i = 0 ; i < elementsLength ; i++ ) {
            element = this.map.getElementAt( this._overlapMapIndexs[i].x, this._overlapMapIndexs[i].y );
            components = element.getOverlapComponents();
            for( var j = 0 ; j < components.length ; j++ ) {
                if( components[j] != this && returnComponents.indexOf( components[j] ) == -1 ) {
                    returnComponents.push( components[j] );
                }
            }
        }
        return returnComponents;
    }
});