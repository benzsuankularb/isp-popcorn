var PropMap = Map.extend({
   
   ctor : function ( mapArray, elementSize ) {
      Map.prototype.ctor.call( this, mapArray, elementSize );
      this.particleLayer = new cc.Sprite();
      this.characters = [];
      this.addChild( this.particleLayer );
   },
   
   runParticle : function ( location, unstableSprite ) {
      unstableSprite.init();
      unstableSprite.setPosition ( location );
      this.particleLayer.addChild ( unstableSprite );
   },
   
   popBubble: function ( bubble ) {
      var sprite = PropFactory.getInstance().getAsset( PropFactory.Assets.POP_BUBBLE );
      sprite.init();
      this.addChild( sprite, bubble.getZOrder() );
      sprite.setPosition( bubble.getPosition() );
      this.removeComponent( bubble )
   },
   
   killCharactersExcept: function ( character ) {
      for ( var i = 0 ; i < this.characters.length ; i++ ) {
	 if( this.characters[ i ] != character ) {
	    this.characters[ i ].squashed();
	 }
      }
   },
   
   addCharacterAt: function ( character , mapIndex ) {
      this.addBlockComponentAt( character , mapIndex );
      this.characters.push( character );
   },
   
   onCharacterDead: function ( character ) {
      this.characters.splice( this.characters.indexOf( character ), 1 );
   }
   
})