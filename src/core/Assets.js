var Assets = cc.Class.extend({
   ctor: function() {
      this.importAtlasToCache();
      this.importAnimationsToCache();
   },
   
   importAtlasToCache: function () {
      var atlasPaths = Assets.FILES.textureAtlases;
      for( var i = 0 ; i <  atlasPaths.length ; i++ ) {
	        SpriteFrameCache.addSpriteFrames( atlasPaths[i].plist, atlasPaths[i].texture );
      }
      var imagesPath = Assets.FILES.images;
      for( var i = 0 ; i <  imagesPath.length ; i++ ) {
         console.log( imagesPath[ i ].path + "    " + imagesPath[ i ].size  );
           SpriteFrameCache.addSpriteFrame( cc.SpriteFrame.create( imagesPath[ i ].path , imagesPath[ i ].size ), imagesPath[ i ].path );

      }
   },
   
   importAnimationsToCache: function () {
      var animationPath = Assets.Animations;
      for ( var i = 0 ; i < animationPath.length ; i++ ) {
         this.importAnimationsToCacheByPrefix( animationPath[i].name, animationPath[i].prefix, animationPath[i].delay, animationPath[i].loop );
         
      }
   },
   
   importAnimationsToCacheByPrefix: function ( name, prefix, delay, loop ) {
      var count = 0;
      var frameArray = [];
      var spriteFrame;
      if ( !SpriteFrameCache.getSpriteFrame( prefix + this.to4Digits( count ) ) ) count++;
      while ( ( spriteFrame = SpriteFrameCache.getSpriteFrame( prefix + this.to4Digits( count++ ) ) )  ) {
         frameArray.push ( spriteFrame );
      }
      AnimationCache.addAnimation( cc.Animation.create( frameArray, delay ), name);
   },
   
   to4Digits: function ( integer ) {
      if ( integer < 0 ) return "0000";
      else if ( integer < 10 )
         return "000" + integer;
      else if ( integer < 100 )
         return "00" + integer;
      return "0" + integer;
   },
   
   setSpriteFrameOffset: function ( spriteFrame, offset, ccp ) {
        spriteFrame.setOffset( cc.p( -spriteFrame.getOriginalSize().width/2, -spriteFrame.getOriginalSize().height/2 ) );
   },
})

Assets.FILES = {
   textureAtlases : [
      { plist: "res/textures/prop_atlas.plist", texture: "res/textures/prop_atlas.png" },
   ],
   images : [
      { path : "res/images/character.png", size: cc.rect(0,0,78,78) }
   ]
}

Assets.SpriteNames = {
   BOMB_GENERAL : "BombBubble",
   BOUNCE_GENERAL : "BounceBubble",
   NORMAL_GENERAL : "GeneralBubble",
   DIAMOND_GENERAL : "marbleOfDoom0001",
   DIAMOND_COMPLETE : "perfectMarbleOfDoom",
   EXPLODE_LINE : "explodeLine",
   EXPLODE_RING : "explodeRing",
   CHARACTER : "res/images/character.png"
}

Assets.AnimationNames = {
   EXPLODE_CENTER : "explodeCenter",
   EXPLODE1 : "explode1",
   EXPLODE2 : "explode2",
   EXPLODE3 : "explode3",
   EXPLODE4 : "explode4",
   DIAMOND_MERGED : "diamondMerged",
   BOMB_ACTIVATED : "bombActivated",
   BOUNCE_UP : "bounceUp",
   BOUNCE_DOWN : "bounceDown",
   BOUNCE_LEFT : "bounceLeft",
   BOUNCE_RIGHT : "bounceRight",
   BUBBLE_POP : "bubblePop"
}

Assets.Animations = [
   { name : Assets.AnimationNames.EXPLODE_CENTER , prefix : "mergeExplode/", delay : 0.04 },
   { name : Assets.AnimationNames.EXPLODE1 , prefix : "Explode1/", delay : 0.05 },
   { name : Assets.AnimationNames.EXPLODE2 , prefix : "Explode2/", delay : 0.05 },
   { name : Assets.AnimationNames.EXPLODE3 , prefix : "Explode3/", delay : 0.05 },
   { name : Assets.AnimationNames.EXPLODE4 , prefix : "Explode4/", delay : 0.05 },
   { name : Assets.AnimationNames.DIAMOND_MERGED , prefix : "marbleOfDoom", delay : 0.03 },
   { name : Assets.AnimationNames.BOMB_ACTIVATED , prefix : "BombBubble", delay : 0.03 },
   { name : Assets.AnimationNames.BOUNCE_UP , prefix : "BounceUp", delay : 0.012 },
   { name : Assets.AnimationNames.BOUNCE_DOWN , prefix : "BounceDown", delay : 0.012 },
   { name : Assets.AnimationNames.BOUNCE_LEFT , prefix : "BounceLeft", delay : 0.012 },
   { name : Assets.AnimationNames.BOUNCE_RIGHT , prefix : "BounceRight", delay : 0.012 },
   { name : Assets.AnimationNames.BUBBLE_POP , prefix : "PopBubble", delay : 0.015 }
]

Assets.getInstance = function(){
   if ( Assets._instance == null ) {
      Assets._instance = new Assets();
   }
}

//prop_atlas : fix bubblepop prefix to 0001 ,replace ".png" ,fix bubble offset