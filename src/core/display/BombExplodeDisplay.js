var BombExplodeDisplay = UnstableDisplay.extend({
   ctor : function () {
      UnstableDisplay.prototype.ctor.call( this );
      this.explodeSprite = new cc.Sprite();
      this.addChild( this.explodeSprite );
      this.explodeSprite.setScale( 1.3 );
   },
   
   init : function () {
      UnstableDisplay.prototype.init.call( this );
      this.explodeSprite.runAction( cc.Sequence.create( cc.Animate.create( this._getAnimation() ), cc.CallFunc.create( this.perform, this) ) );
      this.explodeSprite.setAnchorPoint( 0.5, 0.5 );
      this.explodeSprite.setPosition( cc.p( 0, 0 ) );
   },
   
   _getAnimation : function () {
      BombExplodeDisplay.random ++;
      return AnimationCache.getAnimation( "explode" + ( BombExplodeDisplay.random % 4 + 1 ) );
   }
});

BombExplodeDisplay.random = 1;