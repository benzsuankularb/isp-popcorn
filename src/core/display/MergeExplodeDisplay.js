var MergeExplodeDisplay = UnstableDisplay.extend({
   
   ctor : function () {
      UnstableDisplay.prototype.ctor.call( this );
      this.explodeLines = [];
      this.explodeRings = [];
      this.explodeCenter = new cc.Sprite();
      this.addChild( this.explodeCenter );
      var sprite;
      for ( var i = 0 ; i < MergeExplodeDisplay.EXPLODE_LINE_COUNT ; i++ ) {
	 sprite = this.createExplodeLine();
	 this.explodeLines.push( sprite );
	 this.addChild( sprite );
      }
      for ( var i = 0 ; i < MergeExplodeDisplay.EXPLODE_RING_COUNT ; i++ ) {
	 sprite = cc.Sprite.createWithSpriteFrameName( Assets.SpriteNames.EXPLODE_RING );
	 sprite.setAnchorPoint( 0.5, 0.5 );
	 sprite.setPosition( cc.p( 0, 0 ) );
	 this.explodeRings.push( sprite );
	 this.addChild( sprite );
      }
   },
   
   init : function () {
      UnstableDisplay.prototype.init.call( this );
      this.explodeCenter.runAction( cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.EXPLODE_CENTER ) ) );
      this.explodeCenter.setAnchorPoint( 0.5, 0.5 );
      this.explodeCenter.setPosition( cc.p( 0, 0 ) );
      for ( var i = 0 ; i < MergeExplodeDisplay.EXPLODE_LINE_COUNT ; i++ ) {
         this.randomAndRunExpoldeLine( this.explodeLines[i] );
      }
      var ringDelay = 0.03;
      var ringDegree = Math.random() * ( 360 / 3 );
      for ( var i = 0 ; i < this.explodeRings.length ; i++ ) {
	 this.runExplodeRing( this.explodeRings[i], ringDegree, ringDelay );
	 ringDelay += 0.06;
	 ringDegree += 360 / MergeExplodeDisplay.EXPLODE_RING_COUNT;
      }
      cc.AudioEngine.getInstance().playEffect( Sound.ExplodeFx );
   },
   
   runExplodeRing : function ( explodeRing, degree, delay ) {
      explodeRing.setRotation( degree );
      explodeRing.runAction( cc.Sequence.create( cc.FadeOut.create( 0 ), cc.DelayTime.create( delay ), cc.FadeIn.create( 0 ), cc.ScaleTo.create( 0, 1.3 ), cc.Spawn.create( cc.FadeOut.create( MergeExplodeDisplay.EXPLODE_RING_COUNT_TIME ), cc.ScaleTo.create( MergeExplodeDisplay.EXPLODE_RING_COUNT_TIME, 0.2  ) ), cc.CallFunc.create( this.perform, this) ) );
   },
   
   createExplodeLine : function () {
      var sprite = cc.Sprite.createWithSpriteFrameName( Assets.SpriteNames.EXPLODE_LINE );
      sprite.setAnchorPoint( 0.5, 0 );
      sprite.setPosition( cc.p( 0, 0 ) );
      return sprite;
   },
   
   randomAndRunExpoldeLine : function ( explodeLine ) {
      var randomDegree = Math.random() * 360;
      var targetDegree = randomDegree + Math.random() * 45;
      var time = ( Math.random() * ( MergeExplodeDisplay.MAX_TIME - MergeExplodeDisplay.MIN_TIME ) ) + MergeExplodeDisplay.MIN_TIME;
      explodeLine.setRotation( randomDegree );
      //explodeLine.runAction( cc.Spawn.create( cc.RotateTo.create( time, targetDegree ), cc.FadeOut.create( time ) ) )
      explodeLine.runAction ( cc.Sequence.create( cc.FadeIn.create( 0 ), cc.Spawn.create( cc.RotateTo.create( time, targetDegree ) ), cc.FadeOut.create( time ) ) );
   }
   
});

MergeExplodeDisplay.EXPLODE_RING_COUNT = 3;
MergeExplodeDisplay.EXPLODE_RING_COUNT_TIME = 0.3;
MergeExplodeDisplay.EXPLODE_LINE_COUNT = 15;
MergeExplodeDisplay.MIN_TIME = 0.1;
MergeExplodeDisplay.MAX_TIME = 0.2;