var BounceBubbleDisplay = DisplayObject.extend({
   ctor : function () {
      DisplayObject.prototype.ctor.call( this );
      this.addAnimation( BounceBubbleDisplay.Animations.NORMAL, SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.BOUNCE_GENERAL ) );
      this.addAnimation( BounceBubbleDisplay.Animations.BOUNCE_UP, cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BOUNCE_UP ) ) );
      this.addAnimation( BounceBubbleDisplay.Animations.BOUNCE_DOWN, cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BOUNCE_DOWN ) ) );
      this.addAnimation( BounceBubbleDisplay.Animations.BOUNCE_LEFT, cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BOUNCE_LEFT ) ) );
      this.addAnimation( BounceBubbleDisplay.Animations.BOUNCE_RIGHT, cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BOUNCE_RIGHT ) ) );
      this.init();
   },
   
   init : function () {
      this.setAnimation( BounceBubbleDisplay.Animations.NORMAL );
      
   }

});

BounceBubbleDisplay.Animations = {
   NORMAL : "normal",
   BOUNCE_UP : "b" + BlockComponent.DIR.UP,
   BOUNCE_DOWN : "b" + BlockComponent.DIR.DOWN,
   BOUNCE_LEFT : "b" + BlockComponent.DIR.LEFT,
   BOUNCE_RIGHT : "b" + BlockComponent.DIR.RIGHT ,
}