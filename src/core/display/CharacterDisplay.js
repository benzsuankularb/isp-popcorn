var CharacterDisplay = DisplayObject.extend({
	ctor : function () {
		DisplayObject.prototype.ctor.call( this );
		this.addAnimation( "def", SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.CHARACTER ) );
		this.addAnimation( CharacterDisplay.RIGHT, cc.RotateTo.create( 0, 0 ) );
		this.addAnimation( CharacterDisplay.DOWN, cc.RotateTo.create( 0, 90 ) );
		this.addAnimation( CharacterDisplay.LEFT, cc.RotateTo.create( 0, 180 ) );
		this.addAnimation( CharacterDisplay.UP, cc.RotateTo.create( 0, 270 ) );

		this.setAnimation( "def" );
	}
})

CharacterDisplay.LEFT = "d" + BlockComponent.DIR.LEFT;
CharacterDisplay.RIGHT = "d" + BlockComponent.DIR.RIGHT;
CharacterDisplay.UP = "d" + BlockComponent.DIR.UP;
CharacterDisplay.DOWN = "d" + BlockComponent.DIR.DOWN;