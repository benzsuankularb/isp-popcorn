var PopBubbleDisplay = UnstableDisplay.extend({
   ctor : function () {
      UnstableDisplay.prototype.ctor.call( this );
      this.popSprite = new cc.Sprite();
      this.addChild( this.popSprite );
   },
   
   init : function () {
      UnstableDisplay.prototype.init.call( this );
      this.popSprite.runAction( cc.Sequence.create( cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BUBBLE_POP ) ) ) );
      this.popSprite.setAnchorPoint( 0.5, 0.5 );
      this.popSprite.setPosition( cc.p( 0, 0 ) );
   }
});