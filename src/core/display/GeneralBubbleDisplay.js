var GeneralBubbleDisplay = DisplayObject.extend({
   
   ctor : function () {
      DisplayObject.prototype.ctor.call( this );
      this.addAnimation( GeneralBubbleDisplay.Animations.NORMAL, SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.NORMAL_GENERAL ) );
   },
   
   init : function () {
      this.setAnimation( GeneralBubbleDisplay.Animations.NORMAL );
   }
   
});

GeneralBubbleDisplay.Animations = {
   NORMAL : "normal_general"
}