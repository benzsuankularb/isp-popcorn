var DiamondBubbleDisplay = DisplayObject.extend({
   
   ctor : function () {
      DisplayObject.prototype.ctor.call( this );
      this.stopAction( this.runAction( cc.Sequence.create( cc.TintTo.create( 0, 0, 0, 0 ), cc.TintTo.create( 0.4 ,255, 255, 255 ) ) ) ); //CACHE
      this.addAnimation( DiamondBubbleDisplay.Animations.NORMAL, SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.DIAMOND_GENERAL ) );
      this.addAnimation( DiamondBubbleDisplay.Animations.COMPLETE, SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.DIAMOND_COMPLETE ) );
      this.addAnimation( DiamondBubbleDisplay.Animations.MERGED, cc.RepeatForever.create( cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.DIAMOND_MERGED ) ) ) );
   },
   
   blinkBlack : function () {
      this.runAction( cc.Sequence.create( cc.TintTo.create( 0, 0, 0, 0 ), cc.TintTo.create( 0.4 ,255, 255, 255 ) ) );
   },
   
   init : function () {
      this.setAnimation( DiamondBubbleDisplay.Animations.NORMAL );
      this.blinkBlack();
   }
   
});

DiamondBubbleDisplay.Animations = {
   NORMAL : "normal",
   MERGED : "merged",
   COMPLETE : "complete"
}