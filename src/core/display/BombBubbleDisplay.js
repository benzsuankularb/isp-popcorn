var BombBubbleDisplay = DisplayObject.extend({

   ctor : function () {
      DisplayObject.prototype.ctor.call( this );
      this.addAnimation( BombBubbleDisplay.Animations.NORMAL, SpriteFrameCache.getSpriteFrame( Assets.SpriteNames.BOMB_GENERAL ) );
      this.addAnimation( BombBubbleDisplay.Animations.ACTIVATED, cc.Spawn.create( cc.RepeatForever.create( cc.Animate.create( AnimationCache.getAnimation( Assets.AnimationNames.BOMB_ACTIVATED ) ) ), cc.Sequence.create( /* TODO*/) ) );
   },
   
   init : function() {
      this.setAnimation( BombBubbleDisplay.Animations.NORMAL );
   }
   
});

BombBubbleDisplay.Animations = {
   NORMAL : "normal",
   ACTIVATED : "activated"
}