var BlockComponent = Component.extend({
    
    ctor: function ( map, colliderRad ) {
        Component.prototype.ctor.call( this, map, colliderRad );
        this.movementDirection = BlockComponent.DIR.STAY;
        this._overlapMapIndexs = [];
        this._colliderRad = colliderRad;
        this.scheduleUpdate();
    },
    
    moveWithDirection: function ( dir, amount ) {
        var position = this.getLocation();
        if ( dir == BlockComponent.DIR.UP ) {
            this.setLocation ( cc.p( position.x , position.y + amount ) );
        } else if ( dir == BlockComponent.DIR.DOWN ) {
            this.setLocation ( cc.p( position.x , position.y - amount ) );
        } else if ( dir == BlockComponent.DIR.LEFT ) {
            this.setLocation ( cc.p( position.x - amount, position.y ) );
        } else if ( dir == BlockComponent.DIR.RIGHT ) {
            this.setLocation ( cc.p( position.x + amount, position.y ) );
        }
    },
    
    onCollision: function ( component ) {
        if ( component instanceof Bubble ) {
            this.onCollisionBubble( component );
        }else if ( component instanceof Character ) {
            this.onCollisionCharacter( component );
        }
    },
    
    update: function () {
        this.moveWithDirection( this.movementDirection , this._speed );
    },
    
    getCurrentIndex: function () {
        return this.map.getMapIndexByLocation( this.getLocation() );
    },
    
    moveToMapIndex: function ( mapIndex ) {
        this.setLocation( this.map.getLocationAtMapIndex( mapIndex ) );
    },
    
    stopMove: function () {
        this.moveToMapIndex( this.getCurrentIndex() );
        this.movementDirection = BlockComponent.DIR.STAY;
    },
    
    isMovedOnCenter: function ( amount ) {
        var centerPoint = cc.rectCenter( this.map.getElementAt( this.map.getMapIndexByLocation( this.getLocation() ) ).rectArea );
        var pOffset = cc.pMinus( this.getLocation(), centerPoint );
        return ( this.movementDirection == BlockComponent.DIR.UP && pOffset.x == 0 && pOffset.y >=0 && pOffset.y < amount ) ||
                ( this.movementDirection == BlockComponent.DIR.DOWN && pOffset.x == 0 && pOffset.y <=0 && pOffset.y > -amount ) ||
                ( this.movementDirection == BlockComponent.DIR.LEFT && pOffset.y == 0 && pOffset.x <=0 && pOffset.x > -amount ) ||
                ( this.movementDirection == BlockComponent.DIR.RIGHT && pOffset.y == 0 && pOffset.x >=0 && pOffset.x < amount );
    },
    
    onCollisionBound: function () {
        this.stopMove();
    }
    
});

BlockComponent.isOpporsiteDirection = function ( dir1, dir2 ) {
    return ( dir1 == BlockComponent.DIR.RIGHT && dir2 == BlockComponent.DIR.LEFT ) ||
        ( dir1 == BlockComponent.DIR.LEFT && dir2 == BlockComponent.DIR.RIGHT ) ||
        ( dir1 == BlockComponent.DIR.UP && dir2 == BlockComponent.DIR.DOWN ) ||
        ( dir1 == BlockComponent.DIR.DOWN && dir2 == BlockComponent.DIR.UP );
}

BlockComponent.DIR = {
    STAY: 0,
    UP: 1,
    DOWN: 2,
    LEFT: 3,
    RIGHT: 4
}