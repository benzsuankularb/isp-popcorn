var MapElement = cc.Class.extend ({
    
    ctor: function ( indexX, indexY, map ) {
        this.map = map;
        this.components = [];
        this.reservedBlockComponent;
        this.indexPosition = cc.p( indexX, indexY );
        this.rectArea = new cc.Rect( indexX * map.elementSize ,indexY * map.elementSize ,map.elementSize ,map.elementSize );
    },
    
    getOverlapComponents: function () {
        return this.components;
    },
    
    popOverlapComponents: function ( component ) {
        var index = this.components.indexOf( component );
        if( index > -1) this.components.splice( index );
        this.refreshReservedBlogComponent();
    },
    
    addOverlapComponents: function ( component ) {
        if( this.components.indexOf( component ) == -1 ) {
            this.components.push( component );
        }
        this.refreshReservedBlogComponent();
    },
    
    refreshReservedBlogComponent: function () {
        var length = this.components.length;
        var component;
        this.reservedBlockComponent = null;
        for( var i = 0 ; i < length ; i++) {
            component = this.components[i];
            if ( component instanceof BlockComponent && component.reserveAble) {
                if( cc.pointEqualToPoint( component.getLocation(), cc.rectCenter( this.rectArea ) ) ) {
                    this.reservedBlockComponent = component;
                }
            }
        }
    }
});