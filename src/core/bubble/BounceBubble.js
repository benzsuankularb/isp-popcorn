var BounceBubble = Bubble.extend({
    
    ctor: function ( map, colliderRad , bounceCount) {
        Bubble.prototype.ctor.call( this, map, colliderRad );
        this._bounceCount = this._bounceRemain = bounceCount;
        this._directionTemp;
        this.bounceTime = 0.15;
        this.setBubbleDisplay( new BounceBubbleDisplay() );
        this.init();
        
        if( isDebug ) {
            this.img = TextureCache.addImage("res/debug/Ball.png");
            var sprite = new cc.Sprite();
            sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
            sprite.setOpacity( 150 );
            this.addChild( sprite );
            sprite.runAction( cc.TintTo.create(0, 200, 0, 0) );
        }
    },
    
    endCombo: function () {
        Bubble.prototype.endCombo.call( this );
        this._bounceRemain = this._bounceCount;
    },
    
    activate: function ( character ) {
        this.pop();
    },
    
    activeBehavior: function() {
        if( this._bounceRemain > 0 ) {
            this._bounceRemain--;
            this._directionTemp = this.movementDirection;
            this.stopMove();
            this.runAction( this.getBounceAction() ); //TODO set animation instead.
            this.getBubbleDisplay().setAnimation( "b" + this._directionTemp  );
            cc.AudioEngine.getInstance().playEffect( Sound.SpringFx );
        } else {
            this.stopMove();
            this.endCombo();
        }
    },
    
    getBounceAction: function () {
        var actionInterval = cc.DelayTime.create( this.bounceTime );
        var cbAction = cc.CallFunc.create( this.bounceBack, this );
        return scaleAction = cc.Sequence.create( [ actionInterval, cbAction ] );
    },
    
    bounceBack : function () {
        this.movementDirection = this._directionTemp;
        this._directionTemp = null;
        this.bounce();
    }
    
});