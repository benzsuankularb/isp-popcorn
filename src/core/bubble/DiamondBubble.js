var DiamondBubble = Bubble.extend({
    
    ctor: function ( map, colliderRad, completeCount ) {
        Bubble.prototype.ctor.call( this, map, colliderRad );
        DiamondBubble.count++;
        this.setBubbleDisplay( new DiamondBubbleDisplay() );
        this.init();
        
        if( isDebug ) {
            this.img = TextureCache.addImage("res/debug/Ball.png");
            var sprite = new cc.Sprite();
            sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
            sprite.setOpacity( 150 );
            this.addChild( sprite );
            sprite.runAction( cc.TintTo.create( 0, 0, 0, 300 ) );
        }
    },
    
    init : function () {
        Bubble.prototype.init.call( this );
        this.mergeCount = 1;
    },
    
    activate: function ( character ) {
        cc.AudioEngine.getInstance().playEffect( Sound.ConcreteFx );
    },
    
    activeBehavior: function() {
        this.stopMove();
    },
    
    merge: function ( diamondBubble ) {
        this.mergeCount += diamondBubble.mergeCount;
        this.map.removeComponent( diamondBubble );
        if ( this.isComplete() ) {
            this.map.killCharactersExcept( this._comboBy );
            this.getBubbleDisplay().setAnimation( DiamondBubbleDisplay.Animations.COMPLETE );
        } else {
            this.getBubbleDisplay().setAnimation( DiamondBubbleDisplay.Animations.MERGED );
        }
        this.stopMove();
        this.explode();
        cc.AudioEngine.getInstance().playEffect( Sound.MergeFx );
    },
    
    isComplete: function () {
        return this.mergeCount >= DiamondBubble.count;
    },
    
    explode : function () {
        this.getBubbleDisplay().blinkBlack();
        this.map.runParticle( this.getPosition(), PropFactory.getInstance().getAsset( PropFactory.Assets.MERGE_EPLODE ) );
    },
    
    onCollisionBubble: function ( bubble ) {
        if ( bubble instanceof DiamondBubble && bubble.movementDirection == BlockComponent.DIR.STAY) {
            if ( this.getZOrder() < bubble.getZOrder() ) {
                this.swapZOrder( bubble );
            }
            if ( this.isMovedOnCenter( this._speed ) && cc.pointEqualToPoint( this.getCurrentIndex(), bubble.getCurrentIndex() ) ) {
                this.merge( bubble );
            }
        } else {
            Bubble.prototype.onCollisionBubble.call( this, bubble );
        }
    },
    
    isPushUnAvailable : function ( dir ) {
        var currentIndex = this.map.getMapIndexByLocation( this.getLocation() );
        var nextIndex = this.map.getNextMapIndex( currentIndex, dir );
        var reservedComponent;
        if ( nextIndex )
            reservedComponent = this.map.getElementAt( nextIndex ).reservedBlockComponent;
        return !nextIndex || ( reservedComponent && !(reservedComponent instanceof DiamondBubble) ) || this.isComplete() ;
    }
    
});

DiamondBubble.count = 0;