var Bubble = BlockComponent.extend({
    
    ctor: function ( map, colliderRad ) {
        BlockComponent.prototype.ctor.call( this, map, colliderRad );
        
        this._speed = Bubble.SPEED;
        this.reserveAble = true;
        this.bubbleDisplay;
    },
    
    init : function () {
        this.bubbleDisplay.init();
        this._killCombo = 0;
        this._comboBy = null;
    },
    
    setBubbleDisplay : function ( bubbleDisplay ) {
        if( this.bubbleDisplay ) this.removeChild( this.bubbleDisplay );
        this.bubbleDisplay = bubbleDisplay;
        bubbleDisplay.setAnchorPoint( 0.5, 0.5 );
        this.addChild( bubbleDisplay );
        bubbleDisplay.setPosition( cc.p( 0, 0 ) );
    },
    
    getBubbleDisplay : function () {
        return this.bubbleDisplay;
    },
    
    push: function ( dir, character ) {
        if ( this.isPushUnAvailable( dir ) ) {
            this.activate( character );
        } else {
            this.pushing( character, dir );
        }
    },
    
    isPushUnAvailable : function ( dir ) {
        var currentIndex = this.map.getMapIndexByLocation( this.getLocation() );
        var nextIndex = this.map.getNextMapIndex( currentIndex, dir );
        return !nextIndex || this.map.getElementAt( nextIndex ).reservedBlockComponent;
    },
    
    pushing: function ( character, dir ) {
        this.startCombo( character );
        this.addForce( dir );
        cc.AudioEngine.getInstance().playEffect( Sound.PushFx );
    },
    
    addForce: function ( dir ) {
        this.movementDirection = dir;
    },
    
    startCombo: function ( character ) {
        this._comboBy = character;
        this._killCombo = 0;
    },
    
    endCombo: function () {
        //TODO notify with killCombo number to score system.
        this._comboBy = null;
        this._killCombo = 0;
    },
    
    bounce: function () {
        if ( this.movementDirection != BlockComponent.DIR.STAY) {
            if ( this.movementDirection == BlockComponent.DIR.UP ){
                this.movementDirection = BlockComponent.DIR.DOWN;
            } else if ( this.movementDirection == BlockComponent.DIR.DOWN ){
                this.movementDirection = BlockComponent.DIR.UP;
            } else if ( this.movementDirection == BlockComponent.DIR.LEFT ){
                this.movementDirection = BlockComponent.DIR.RIGHT;
            } else if ( this.movementDirection == BlockComponent.DIR.RIGHT ){
                this.movementDirection = BlockComponent.DIR.LEFT;
            }
        }
    },
    
    onCollisionBubble: function ( bubble ) {
        if ( bubble.movementDirection != BlockComponent.DIR.STAY && this.movementDirection != BlockComponent.DIR.STAY ) {
            this.onBounceWithBubble();
        } else {
            this.activeBehavior();
        }
    },
    
    onBounceWithBubble: function () {
        this.bounce();
        cc.AudioEngine.getInstance().playEffect( Sound.BounceFx );
    },
    
    onCollisionCharacter: function ( character ) {
        //this.behavior.onCollisionCharacter( component ); //Now unimportant
        if ( this._comboBy && this.movementDirection != BlockComponent.DIR.STAY ) {
            this.killCharacter( character );
        }
    },
    
    onCollisionBound: function () {
        this.activeBehavior();
        BlockComponent.prototype.onCollisionBound.call( this );
    },
    
    killCharacter: function ( character ) {
        this._killCombo++;
        character.squashed();
        //Notify score
    },
    
    pop: function () {
        this.map.popBubble ( this );
        cc.AudioEngine.getInstance().playEffect( Sound.PopFx );
    },
    
    activate: function ( character ) {
        console.warn('Abstract not implemented activate()');
    },
    
    activeBehavior: function() {
        console.warn('Abstract not implemented activeBehavior()');
    }
    
});

Bubble.SPEED = 19;

Bubble.Animation = {
    general : 'general',
    pop : 'pop'
}