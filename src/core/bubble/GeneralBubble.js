var GeneralBubble = Bubble.extend({
    
    ctor: function ( map, colliderRad ) {
        Bubble.prototype.ctor.call( this, map, colliderRad );
        this.setBubbleDisplay( new GeneralBubbleDisplay() );
        this.init();
        
        if( isDebug ) {
            this.img = TextureCache.addImage("res/debug/Ball.png");
            var sprite = new cc.Sprite();
            sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
            sprite.setOpacity( 150 );
            this.addChild( sprite );
        }
    },
    
    activate: function ( character ) {
        this.pop();
    },
    
    activeBehavior: function() {
        this.stopMove();
        this.endCombo();
    }
    
});