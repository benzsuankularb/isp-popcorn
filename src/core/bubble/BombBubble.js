var BombBubble = Bubble.extend ({
    
    ctor: function ( map, colliderRad , bombArray ) {
        Bubble.prototype.ctor.call( this, map, colliderRad );
        this._bombArray = bombArray;
        this.setBubbleDisplay( new BombBubbleDisplay() );
        this.init();
        
        if( isDebug ) {
            this.img = TextureCache.addImage("res/debug/Ball.png");
            var sprite = new cc.Sprite();
            sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
            sprite.setOpacity( 150 );
            this.addChild( sprite );
            sprite.runAction( cc.TintTo.create(0, 0, 200, 0) );
        }
    },
    
    init : function() {
        Bubble.prototype.init.call( this );
        this._isSparked = false;
    },
    
    endCombo: function () {
        Bubble.prototype.endCombo.call( this );
        this._bounceRemain = this._bounceCount;
    },
    
    activate: function ( character ) {
        this.sparkWithTime( character, BombBubble.SPARK_TIME );
    },
    
    sparkWithTime: function ( character, t ) {
        if( !this._isSparked ) {
            this.startCombo( character );
            this._isSparked = true;
            var delayAction = cc.DelayTime.create( t );
            var cbAction = cc.CallFunc.create( this.bomb, this );
            this.runAction( cc.Sequence.create( [ delayAction, cbAction ] ) );
            this.bubbleDisplay.setAnimation( BombBubbleDisplay.Animations.ACTIVATED );
            cc.AudioEngine.getInstance().playEffect( Sound.MatchFx );
        }
    },
    
    pop: function () {
        //Animate pop
    },
    
    bombed: function() {
        this.endCombo();
        this.map.removeComponent ( this );
    },
    
    bomb: function () {
        var sequenceArray = [];
        this.pop()
        for ( var i = 0 ; i < this._bombArray.length ; i++ ) {
            sequenceArray.push( cc.DelayTime.create( 0.08 ) );
            sequenceArray.push( cc.CallFunc.create( this.bombAtLayer, this, i ) );
        }
        sequenceArray.push( cc.CallFunc.create( this.bombed, this ) );
        this.runAction( cc.Sequence.create( sequenceArray ) );
        cc.AudioEngine.getInstance().playEffect( Sound.BoomFx );
    },
    
    bombAtLayer: function ( callFrom, layer ) {
        var currentIndex = this.getCurrentIndex();
        var i, j;
        var bombLayer = this._bombArray[ layer ];
        var length = bombLayer.length;
        for ( i = 0 ; i < length ; i++ ) {
            this.bombAtElement( cc.pAdd( currentIndex, cc.p( bombLayer[i][0], bombLayer[i][1] ) ) );
        }
    },
    
    bombAtElement: function ( index ) {
        var element = this.map.getElementAt( index );
        if ( !element ) return;
        var components = element.getOverlapComponents();
        //Add unstable Animation (BOMB)
        this.map.runParticle( this.map.getLocationAtMapIndex( index ), PropFactory.getInstance().getAsset( PropFactory.Assets.BOMB_EPLODE ) );
        for( var i = 0 ; i < components.length ; i++ ) {
            if ( components[i] instanceof Character ) {
                this.killCharacter( components[i] );
            }
        }
    },
    
    activeBehavior: function() {
        this.stopMove();
        this.bomb();
    }
    
});

BombBubble.SPARK_TIME = 1.5;
BombBubble.BOMB_ARRAY1 = [
    [ [0,0] ],
    [ [-1,0], [-1,-1], [-1,1], [0,1], [0,-1], [1,0], [1,-1], [1,1] ]
    //[/*third bomb*/] //..
]