var Character = BlockComponent.extend({
    
    ctor: function ( map, colliderRad, speed, priority ) {
        BlockComponent.prototype.ctor.call( this, map, colliderRad );
        this._faceDirection = BlockComponent.DIR.RIGHT;
        this._nextDirection = BlockComponent.DIR.STAY;
        this._display = new CharacterDisplay();
        this._speed = speed;
        this._priority = priority;
        this.reserveAble = false;
        this._display.setAnchorPoint( 0.5, 0.5 );
        this.addChild( this._display );

        if( false ) {//isDebug ) {
            this.img = TextureCache.addImage("res/debug/Ball.png");
            var sprite = new cc.Sprite();
            sprite.initWithTexture( this.img, cc.rect( 0, 0, this.img.getPixelsWide() , this.img.getPixelsHigh() ) );
            sprite.setOpacity( 150 );
            this.addChild( sprite );
            sprite.runAction( cc.TintTo.create( 0, 200, 200, 200 ) );
        }
    },
    
    push: function () {
        if( this._isReady() ) {
            var targetIndex = this.map.getNextMapIndex( this.getCurrentIndex(), this._faceDirection );
            var targetElement;
            if( targetIndex ) {
                targetElement = this.map.getElementAt( targetIndex );
                if( targetElement.reservedBlockComponent instanceof Bubble ) {
                    targetElement.reservedBlockComponent.push( this._faceDirection, this );
                }
            } else {
                cc.AudioEngine.getInstance().playEffect( Sound.ConcreteFx );
            }
            //animate push
        }
    },
    
    activate: function () {
        if( this._isReady() ) {
            var targetIndex = this.map.getNextMapIndex( this.getCurrentIndex(), this._faceDirection );
            var targetElement;
            if( targetIndex ) {
                targetElement = this.map.getElementAt( targetIndex );
                if( targetElement.reservedBlockComponent instanceof Bubble ) {
                    targetElement.activate();
                }
            }
            //animate activate
        }
    },
    
    power: function ( bool ) {
    },
    
    setNextDirection: function ( dir ) {
        if( dir != BlockComponent.DIR.STAY ) this._setFaceDirection( dir );
        this._nextDirection = dir;
    },
    
    moveWithDirection: function ( dir, amount ) {
        BlockComponent.prototype.moveWithDirection.call( this, dir, amount );
        if ( this.movementDirection == BlockComponent.DIR.STAY || BlockComponent.isOpporsiteDirection( this.movementDirection, this._nextDirection ) ) {
            this._setDirection( this._nextDirection );
        } else if ( this.isMovedOnCenter( amount ) && this._nextDirection != this.movementDirection ) {
            this.stopMove();
            this._setDirection( this._nextDirection );
        }
    },
    
    _isReady: function () {
        return this.map.isPointCenterToIndex( this.getLocation() );// && this.movementDirection == BlockComponent.DIR.STAY; 
    },
    
    _setDirection: function ( dir ) {
        if( this.movementDirection != dir ) this.movementDirection = dir;
    },
    
    _setFaceDirection : function (dir) {
        this._faceDirection = dir;
        this._display.setAnimation( "d" + dir );
    },
    
    onCollisionBubble: function ( bubble ) {
        if( bubble.movementDirection == BlockComponent.DIR.STAY ) {
            this.stopMove();
        } else {
            bubble.killCharacter( this );
        }
    },
    
    onCollisionCharacter: function ( character ) {
        /*if( this._priority < character.priority ) {
            this.catched();
        }*/
    },
    
    squashed: function() {
        this.map.removeComponent( this );
        this.onDead();
        cc.AudioEngine.getInstance().playEffect( Sound.DieFx );
        cc.AudioEngine.getInstance().playEffect( Sound.SquashFx );
        //Animate squashed
    },
    
    catched: function() {
        
    },
    
    onDead: function () {
        this.map.onCharacterDead( this );
    }
});

BlockComponent.DIR = {
    STAY: 0,
    UP: 1,
    DOWN: 2,
    LEFT: 3,
    RIGHT: 4
}