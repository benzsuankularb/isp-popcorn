var PropFactory = cc.Class.extend({
   
   ctor : function () {
      this._assets = {}
      for ( var i = 0 ; i < PropFactory.AssetsArray.length ; i++ ) {
	 var array = [];
	 for ( var j = 0 ; j < PropFactory.AssetsArray[i].COUNT ; j++ ) {
	    array.push( PropFactory.AssetsArray[i].ctor() );
	 }
	 this._assets[ PropFactory.AssetsArray[i].NAME ] = array;
      }
   },
   
   getAsset : function ( assetInfo ) {
      var array = this._assets[ assetInfo.NAME ];
      if ( !array || array.length == 0 ) return null;
      return array.pop();
   },
   
   addAsset : function ( assetInfo, obj ) {
      
      this._assets[ assetInfo.NAME ].push( obj );
   }

});

PropFactory._instance;

PropFactory.getInstance = function () {
   if ( !PropFactory._instance )
       PropFactory._instance = new PropFactory();
   return PropFactory._instance;      
}

PropFactory.Assets = {
   POP_BUBBLE : { NAME: "popBubble", COUNT : 100, ctor: function() { return new PopBubbleDisplay() } },
   MERGE_EPLODE : { NAME: "mergeExplode", COUNT : 5, ctor: function() { return new MergeExplodeDisplay() } },
   BOMB_EPLODE : { NAME: "bombExplode", COUNT : 50, ctor: function() { return new BombExplodeDisplay() } }
}

PropFactory.AssetsArray = [
   PropFactory.Assets.POP_BUBBLE,
   PropFactory.Assets.MERGE_EPLODE,
   PropFactory.Assets.BOMB_EPLODE
]