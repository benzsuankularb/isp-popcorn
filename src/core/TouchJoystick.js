var TouchJoystick = cc.Class.extend({
    
    ctor: function ( character ) {
        this._character = character;
        this._directionTouch;
        this.ACCEPT_DELTA = 10;
    },
    
    onTouchesBegan: function ( touch, event ) {
        if ( this.isLeftHalf( touch ) ) {
            this.onLeftTouchBegin( touch );
        } else {
            this.onActionTouchBegin();
        }
    },
    
    onTouchesEnded: function ( touch, event ) {
        if ( this._directionTouch && this._directionTouch.getId() == touch.getId() ) {
            this.onDirectionTouchEnded();
        }
    },
    
    onTouchesMoved: function ( touch, event ) {
        if ( this._directionTouch && this._directionTouch.getId() == touch.getId() ) {
            this.onDirectionTouchMoved( touch );
        }
    },
    
    onActionTouchBegin: function () {
        this._character.push();
    },
    
    onDirectionTouchBegin: function ( touch ) {
        if ( !this._directionTouch ) this._directionTouch = touch;
    },
    
    onDirectionTouchMoved: function ( touch ) {
        if ( cc.pointsMagnitute( cc.p( 0, 0 ), touch.getDelta() ) >= this.ACCEPT_DELTA ) {
            var degree = cc.degreeOfZeroToPoint( touch.getDelta() );
            if ( degree >= TouchJoystick.TOUCH_DEGREE.RIGHT && degree <= TouchJoystick.TOUCH_DEGREE.UP ) {
                
            }
            if ( degree >= TouchJoystick.TOUCH_DEGREE.RIGHT ) {
                
            }
            if ( degree >= TouchJoystick.TOUCH_DEGREE.RIGHT ) {
                
            }
        }
        
    },
    
    onDirectionTouchEnded: function () {
        this._character.setNextDirection( BlockComponent.DIR.STAY );
    },
    
    isLeftHalf: function ( touch ) {
        var gameArea = GameProperties.CONTENT_SIZE;
        return touch.x <= gameArea.width/2;
    }
});

TouchJoystick.TOUCH_DEGREE = {
    RIGHT: 45,
    DOWN: 135,
    LEFT: 225,
    UP: 315
}